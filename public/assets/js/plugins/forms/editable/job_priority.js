(function ($) {
    "use strict";
    var JobPriority = function (options) {
        this.init('jobpriority', options, JobPriority.defaults);
    };
    $.fn.editableutils.inherit(JobPriority, $.fn.editabletypes.abstractinput);
    $.extend(JobPriority.prototype, {
        render: function() {
            this.$input = this.$tpl.find('select');
        },
        value2html: function(value, element) {
            var html = '';
            if(value == 0){
                html = '<span class="text-green-700">Low</span>';
            }else if(value == 1){
                html = '<span class="text-info">Medium</span>';
            }else if(value == 2){
                html = '<span class="text-danger">High</span>';
            }
            $(element).html(html); 
        },
        
        html2value: function(html) {        
            return null;  
        },
        
       /**
        Converts value to string. 
        It is used in internal comparing (not for sending to server).
        
        @method value2str(value)  
       **/
        value2str: function(value) {
           return value;
       }, 
       
       /*
        Converts string to value. Used for reading value from 'data-value' attribute.
        
        @method str2value(str)  
       */
       str2value: function(str) {
           /*
           this is mainly for parsing value defined in data-value attribute. 
           If you will always set value by javascript, no need to overwrite it
           */
           return str;
       },                
       
       /**
        Sets value of input.
        
        @method value2input(value) 
        @param {mixed} value
       **/         
       value2input: function(value) {
            this.$input.filter('[name="job_priority"]').val(value);
       },       
       
       /**
        Returns value of input.
        
        @method input2value() 
       **/          
       input2value: function() { 
           return this.$input.filter('[name="job_priority"]').val();
       },        
       
        /**
        Activates input: sets focus on the first field.
        
        @method activate() 
       **/        
       activate: function() {
            /*this.$input.filter('[name="status"]').focus();*/
       },  
       
       /**
        Attaches handler to submit form in case of 'showbuttons=false' mode
        
        @method autosubmit() 
       **/       
       autosubmit: function() {
           this.$input.keydown(function (e) {
                if (e.which === 13) {
                    $(this).closest('form').submit();
                }
           });
       }       
    });

    JobPriority.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
        tpl:'<div class="control-group form-group">'+
            '<div class="input-group">'+
            '<select class="form-control" name="job_priority"><option value="0">Low</option><option value="1">Medium</option><option value="2">High</option></select>'+
            '<div class="input-group-btn"><button type="submit" class="btn btn-primary btn-icon editable-submit"><i class="icon-check"></i></button>'+
            '<button type="button" class="btn btn-default btn-icon editable-cancel"><i class="icon-x"></i></button></div></div>',

        /*
        tpl: '<div class="checklist-item"><label><span style="width: 120px;">Status: </span><select name="inspection_status" id="inspection_status" style="width:100px;" class="input"><option value="1">-</option><option value="2">Pass</option><option value="3">Fail</option><option value="4">Canceled</option></select></label></div>'+
             '<div class="checklist-item"><label><span style="width: 120px;">Completed On: </span><input autocomplete="off" type="text" id="completed_date" name="completed_date" class="input form-control"></label></div>'+
             '<div class="checklist-item"><label><span style="width: 120px;">Remarks: </span><input type="text" name="remarks" id="remarks" class="input form-control"></label></div>'+
             '<div class="checklist-item"><label><span style="width: 120px;">Inspection No: </span><input type="text" id="inspection_no" name="inspection_no" class="input form-control "></label></div>',
        */   
        inputclass: ''
    });

    $.fn.editableform.buttons = '';

    $.fn.editabletypes.jobpriority = JobPriority;


}(window.jQuery));