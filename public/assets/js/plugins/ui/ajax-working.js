(function($){
	$.showWorking = function(msg, colour){
	   	if($('#workingOverlay').length){
			// A working is already shown on the page:
			return false;
		}
		var prg_colour = 'progress-bar-success';
		prg_colour = typeof colour !== 'undefined' ? 'progress-bar-' + colour  : 'progress-bar-success';

        var markup1 = '<div id="workingOverlay" class="modal-backdrop-white fade in" style="z-index: 100000;"></div>';
        var markup2 ='<div id="prgress" class="modal-scrollable" style="z-index: 100000; display:none;"><div class="loading-spinner in" style="width: 200px; margin-left: -100px; z-index: 100010;"><div class="loading-spinner-text">' + msg + '</div><div class="progress progress-striped active"><div class="progress-bar ' + prg_colour + '" style="width: 100%;"></div></div></div></div>';
		
		$(markup1).appendTo('body');
		$(markup2).appendTo('body').fadeIn();
	}
	$.hideWorking = function(){
		$('#prgress').fadeOut(200,function(){
			/*$('#prgress').fadeOut(200,function(){
				$(this).remove()
			});*/
			$('#workingOverlay').remove();
			$(this).remove();
		});
	}
	$.showSessionExpired = function(){
        if($('#workingOverlay').length){
            $.hideWorking();
        }
        var msg = '<p>Your Login Session has expired.</p><p><a href="#" onclick="window.location.reload(true);">Please click here to reload</a></p>';
        var markup1 = '<div id="workingOverlay" class="modal-backdrop-white fade in" style="z-index: 1050;"></div>';
        var markup2 ='<div id="prgress" class="modal-scrollable" style="z-index: 1050; display:none;"><div class="loading-spinner in" style="width: 400px; margin-left: -100px; z-index: 1060;background-color: #F6A8A8;padding: 20px;-webkit-border-radius: 6px;  -moz-border-radius: 6px;  border-radius: 6px;border: 1px solid #A34C4C;font-size: 20px;"><div class="loading-spinner-text">' + msg + '</div></div></div>';
        
        $(markup1).appendTo('body');
        $(markup2).appendTo('body').fadeIn();
    }
})(jQuery);

function jGrowl_success($header, $message){
    /*$.jGrowl($message, {
			header: $header,
            life: 5000,
			theme: 'success'
		});*/
	$message = '<strong>' + $header +'</strong><br>'+$message;
	$.bootstrapGrowl($message, {
		ele: 'body', // which element to append to
		type: 'success', // (null, 'info', 'danger', 'success')
		offset: {from: 'top', amount: 100}, // 'top', or 'bottom'
		align: 'right', // ('left', 'right', or 'center')
		width: 250, // (integer, or 'auto')
		delay: 5000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
		allow_dismiss: true, // If true then will display a cross to close the popup.
		stackup_spacing: 10 // spacing between consecutively stacked growls.
	});
}
function jGrowl_error($header, $message){
    /*$.jGrowl($message, {
			header: $header,
            life: 5000,
			theme: 'danger'
		});*/
	$message = '<strong>' + $header +'</strong><br>' + $message;
	$.bootstrapGrowl($message, {
		ele: 'body', // which element to append to
		type: 'danger', // (null, 'info', 'danger', 'success')
		offset: {from: 'top', amount: 100}, // 'top', or 'bottom'
		align: 'right', // ('left', 'right', or 'center')
		width: 250, // (integer, or 'auto')
		delay: 5000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
		allow_dismiss: true, // If true then will display a cross to close the popup.
		stackup_spacing: 10 // spacing between consecutively stacked growls.
	});
}

function jGrowl_json_error($header, $errors){
    
	$message = '<strong>' + $header +'</strong><br><ul>';
	$.each($errors, function( key, value ) {
        $message += '<li>' + value[0] + '</li>'; //showing only the first error.
    });
	$message += '</ul>';

	$.bootstrapGrowl($message, {
		ele: 'body', // which element to append to
		type: 'danger', // (null, 'info', 'danger', 'success')
		offset: {from: 'top', amount: 50}, // 'top', or 'bottom'
		align: 'right', // ('left', 'right', or 'center')
		width: 250, // (integer, or 'auto')
		delay: 5000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
		allow_dismiss: true, // If true then will display a cross to close the popup.
		stackup_spacing: 10 // spacing between consecutively stacked growls.
	});
}