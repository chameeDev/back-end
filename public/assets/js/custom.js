/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/custom.js":
/*!********************************!*\
  !*** ./resources/js/custom.js ***!
  \********************************/
/***/ (() => {

$(function () {
  window.setTimeout(function () {
    $(".alert-dismiss").slideUp(500, function () {
      $(".alert-dismiss").alert('close');
    });
  }, 3000);
  $('.select2').select2({
    minimumResultsForSearch: 5
    /*width: 'auto'*/

  });
  $.ajaxSetup({
    cache: false,
    error: function error(xhr, status, err) {
      if (xhr.status == 401) {
        window.location.href = APP.siteUrl;
      }
    }
  });
  $('.styled').uniform({
    radioClass: 'choice'
  });
  $(window).on('scroll', function () {
    if ($(this).scrollTop() >= 50) {
      // If page is scrolled more than 50px
      $('#return-to-top').fadeIn(200); // Fade in the arrow
    } else {
      $('#return-to-top').fadeOut(200); // Else fade out the arrow
    }
  });
  $('#return-to-top').click(function () {
    // When arrow is clicked
    $('body,html').animate({
      scrollTop: 0 // Scroll to top of body

    }, 500);
  });
  var switchery_w = Array.prototype.slice.call(document.querySelectorAll('.switchery-warning'));
  switchery_w.forEach(function (html) {
    var switchery = new Switchery(html, {
      color: '#FF7043'
    });
  });
  switchery_w = Array.prototype.slice.call(document.querySelectorAll('.switchery-success'));
  switchery_w.forEach(function (html) {
    var switchery = new Switchery(html);
  });
  switchery_w = Array.prototype.slice.call(document.querySelectorAll('.switchery-danger'));
  switchery_w.forEach(function (html) {
    var switchery = new Switchery(html, {
      color: '#EF5350'
    });
  });

  if ($('.datepicker').length > 0) {
    $('.datepicker').datepicker({
      format: "dd-M-yyyy",
      todayBtn: "linked",
      autoclose: true,
      todayHighlight: true
    });
  }

  getNotifications();
  setInterval(function () {
    getNotifications();
  }, 30000);
});
$(document).ajaxError(function (event, jqxhr, settings, thrownError) {
  if (jqxhr.status === 401) {
    console.log('Your session has expired. Please refresh the page.'); // alert('Your session has expired. Please refresh the page.');
    //$.showSessionExpired();

    return false;
  }
});

$.fn.dataTable.ext.errMode = function (settings, techNote, message) {
  console.log(message);
};

getNotifications = function getNotifications() {
  $.ajax({
    url: APP.siteUrl + 'notifications',
    type: "GET",
    dataType: 'json',
    success: function success(data) {
      if (data.status) {
        if (data.count > 0) {
          $('#li-notification-count').html(data.count);
          $('#ul-notification-list').html('');
          data.notifications.forEach(function (n) {
            var li = $('<li class="media">' + '<div class="media-left">' + '<i class=" icon-quill2"></i>' + '</div>' + '<div class="media-body">' + '<a href="' + (n.url ? n.url : '#') + '" class="media-heading">' + '<span class="text-semibold">' + n.title + '</span>' + '<span class="media-annotation pull-right">' + n.created_at + '</span>' + '</a>' + '<span class="text-muted">' + n.message + '</span>' + '</div>' + '</li>');
            $('#ul-notification-list').append(li);
          });
        } else {
          $('#li-notification-count').html('');
          $('#ul-notification-list').html('');
        }
      }
    }
  });
};

markNotificationAsRead = function markNotificationAsRead(type, id) {
  var url = APP.siteUrl + 'notifications/' + type + '/' + id + '/read';
  $.ajax({
    type: 'POST',
    url: url,
    data: $('#form-notification-read').serialize(),
    dataType: "json",
    success: function success(rtn) {
      if (rtn.status == true) {
        getNotifications();
      } else {
        $.jGrowl_error('Failed', rtn.msg);
      }
    },
    error: function error(jq, status, _error) {
      $.jGrowl_error('Failed', 'An error occured while updating.');
    }
  });
};

getFullDirList = function (_getFullDirList) {
  function getFullDirList(_x, _x2) {
    return _getFullDirList.apply(this, arguments);
  }

  getFullDirList.toString = function () {
    return _getFullDirList.toString();
  };

  return getFullDirList;
}(function (dirs, initial_path) {
  var list = [];
  dirs.forEach(function (d) {
    d.path = initial_path + d.name;
    list.push(d);

    if (d.sub_dirs && d.sub_dirs.length > 0) {
      list = list.concat(getFullDirList(d.sub_dirs, d.path + '/'));
    }
  });
  return list;
});

escapeRegExp = function escapeRegExp(string) {
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
};

arrangeIntoTree = function arrangeIntoTree(paths) {
  // Adapted from http://brandonclapp.com/arranging-an-array-of-flat-paths-into-a-json-tree-like-structure/
  var tree = [];

  for (var i = 0; i < paths.length; i++) {
    var path = paths[i];
    var currentLevel = tree;

    for (var j = 0; j < path.length; j++) {
      var part = path[j];
      var existingPath = findWhere(currentLevel, 'text', part); //var icon =  (j == path.length - 1 ? 'icon-file-text' : 'icon-folder-open');

      var icon = 'icon-file-text text-info';

      if (existingPath) {
        existingPath.icon = 'icon-folder-open3 text-warning-600';
        currentLevel = existingPath.children;
      } else {
        var newPart = {
          text: part,
          children: [],
          icon: icon
        };
        currentLevel.push(newPart);
        currentLevel = newPart.children;
      }
    }
  }

  return tree;

  function findWhere(array, key, value) {
    // Adapted from https://stackoverflow.com/questions/32932994/findwhere-from-underscorejs-to-jquery
    t = 0; // t is used as a counter

    while (t < array.length && array[t][key] !== value) {
      t++;
    }

    ; // find the index where the id is the as the aValue

    if (t < array.length) {
      return array[t];
    } else {
      return false;
    }
  }
};

convertDirStructureAsJsTree = function (_convertDirStructureAsJsTree) {
  function convertDirStructureAsJsTree(_x3) {
    return _convertDirStructureAsJsTree.apply(this, arguments);
  }

  convertDirStructureAsJsTree.toString = function () {
    return _convertDirStructureAsJsTree.toString();
  };

  return convertDirStructureAsJsTree;
}(function (structure) {
  var rtn = [];
  structure.forEach(function (d) {
    var nd = {
      text: d.name
    };

    if (d.sub_dirs) {
      nd.children = convertDirStructureAsJsTree(d.sub_dirs);
      nd.state = {
        opened: true
      };
    }

    rtn.push(nd);
  });
  return rtn;
});

generateUUID = function generateUUID() {
  // Public Domain/MIT
  var d = new Date().getTime(); //Timestamp

  var d2 = performance && performance.now && performance.now() * 1000 || 0; //Time in microseconds since page-load or 0 if unsupported

  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = Math.random() * 16; //random number between 0 and 16

    if (d > 0) {
      //Use timestamp until depleted
      r = (d + r) % 16 | 0;
      d = Math.floor(d / 16);
    } else {
      //Use microseconds since page-load if supported
      r = (d2 + r) % 16 | 0;
      d2 = Math.floor(d2 / 16);
    }

    return (c === 'x' ? r : r & 0x3 | 0x8).toString(16);
  });
  return 'id-' + uuid;
};

/***/ }),

/***/ "./resources/css/app.css":
/*!*******************************!*\
  !*** ./resources/css/app.css ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"/assets/js/custom": 0,
/******/ 			"css/app": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunk"] = self["webpackChunk"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	__webpack_require__.O(undefined, ["css/app"], () => (__webpack_require__("./resources/js/custom.js")))
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["css/app"], () => (__webpack_require__("./resources/css/app.css")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;