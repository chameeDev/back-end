/* ------------------------------------------------------------------------------
 *
 *  # Echarts - chart combinations
 *
 *  Chart combination configurations
 *
 *  Version: 1.0
 *  Latest update: August 1, 2015
 *
 * ---------------------------------------------------------------------------- */

$(function () {

    // Set paths
    // ------------------------------

    require.config({
        paths: {
            echarts: 'assets/js/plugins/visualization/echarts'
        }
    });



    // Configuration
    // ------------------------------

    require(

        // Add necessary charts
        [
          'echarts',
          'echarts/theme/limitless',
          'echarts/chart/line',
          'echarts/chart/bar',
          'echarts/chart/pie',


          'echarts/chart/scatter',
          'echarts/chart/k',
          'echarts/chart/radar',
          'echarts/chart/gauge'
        ],


        // Charts setup
        function (ec, limitless) {


            // Initialize charts
            // ------------------------------
            var connect_pie = ec.init(document.getElementById('connect_pie'), limitless);
            var connect_column = ec.init(document.getElementById('connect_column'), limitless);

            //
            // Column and pie connection
            //


            //Pie chart pre-process
            var piechart_array = [{value: 0, name: "Yet To Start"}, {value: 0, name: "In Progress"}, {value: 0, name: "Pending"}, {value: 0, name: "On-Hold"}];

            for (var i = 0; i < piechart_array.length; i++) { 
                $.each(task_files, function(idx, obj) {
                    if(obj.file_status == 'I' && piechart_array[i].name == 'In Progress'){
                        piechart_array[i].value = obj.files_count;
                        piechart_array[i].name='Ongoing';
                    }else if(obj.file_status == 'Y' && piechart_array[i].name == 'Yet To Start'){
                        piechart_array[i].value = obj.files_count;
                        piechart_array[i].name='Pipeline';
                    }else if(obj.file_status == 'O' && piechart_array[i].name == 'On-Hold'){
                        piechart_array[i].value = obj.files_count;
                    }else if(obj.file_status == 'P' && piechart_array[i].name == 'Pending'){
                        piechart_array[i].value = obj.files_count;
                        piechart_array[i].name='Pause';
                    }
                });
            }

            var in_valid_keys = [];

            for (var i = 0; i < piechart_array.length; i++) { 
                if(piechart_array[i].value == 0){
                    in_valid_keys.push(piechart_array[i].name);
                }
            }

            for (var i = 0; i < in_valid_keys.length; i++){
                piechart_array = jQuery.grep(piechart_array, function(value) {
                    return value.name != in_valid_keys[i];
                  });
            }
            //end pie chart pre-process

            //Column chart pre-process
            var column_chart_data = [];

            var t_yet_to_start_array = [];
            var t_in_progress_array = [];
            var t_pending_array = [];
            var t_on_hold_array = [];

            $.each(task_v_files, function(idx, obj) {
                if(jQuery.inArray(obj.name, column_chart_data) == -1){
                    column_chart_data.push(obj.name);
                }
            });

            for(var i = 0; i < column_chart_data.length; i++){
                var matches_y = $.grep(task_v_files, function(e) { return e.file_status == 'Y' && e.name == column_chart_data[i]});
                var matches_i = $.grep(task_v_files, function(e) { return e.file_status == 'I' && e.name == column_chart_data[i]});
                var matches_p = $.grep(task_v_files, function(e) { return e.file_status == 'P' && e.name == column_chart_data[i]});
                var matches_o = $.grep(task_v_files, function(e) { return e.file_status == 'O' && e.name == column_chart_data[i]});

                if(matches_y.length > 0){
                    var total = 0;
                    $.each(matches_y, function(n, t){
                        total += t.files_count;
                    });
                    t_yet_to_start_array.push(total);
                }else{
                    t_yet_to_start_array.push(0);
                }

                if(matches_i.length > 0){
                    var total = 0;
                    $.each(matches_i, function(n, t){
                        total += t.files_count;
                    });
                    t_in_progress_array.push(total);
                }else{
                    t_in_progress_array.push(0);
                }

                if(matches_p.length > 0){
                    var total = 0;
                    $.each(matches_p, function(n, t){
                        total += t.files_count;
                    });
                    t_pending_array.push(total);
                }else{
                    t_pending_array.push(0);
                }

                if(matches_o.length > 0){
                    var total = 0;
                    $.each(matches_o, function(n, t){
                        total += t.files_count;
                    });
                    t_on_hold_array.push(total);
                }else{
                    t_on_hold_array.push(0);
                }
            }

            var columnchart_array = [
                {
                    name: 'Pipeline',
                    type: 'bar',
                    stack: 'Total',
                    data: t_yet_to_start_array
                },
                {
                    name: 'Ongoing',
                    type: 'bar',
                    stack: 'Total',
                    data: t_in_progress_array
                },
                {
                    name: 'Pause',
                    type: 'bar',
                    stack: 'Total',
                    data: t_pending_array
                },
                {
                    name: 'On-Hold',
                    type: 'bar',
                    stack: 'Total',
                    data: t_on_hold_array
                }
            ];

            //end column chart pre-process

            // Pie options
            connect_pie_options = {

                // Add title
                title: {
                    text: 'File Status',
                    subtext: 'Current file status data',
                    x: 'center'
                },

                // Add tooltip
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },

                // Add legend
                /*legend: {
                    orient: 'vertical',
                    x: 'left',
                    data: ['Pipeline','Ongoing']
                }*/

                // Enable drag recalculate
                calculable: true,

                // Add series
                series: [{
                    name: 'File status',
                    type: 'pie',
                    radius: '75%',
                    center: ['50%', '57.5%'],
                    data: piechart_array
                }]
            };

            // Column options
            connect_column_options = {
                options: {
                    responsive: true,
                    maintainAspectRatio: true,
                    scales: {
                        xAxis: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                },
                // Setup grid
                // grid: {
                //     x: 40,
                //     x2: 47,
                //     y: 35,
                //     y2: 25
                // },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },

                // Add legend
                legend: {
                    data: ['Pipeline','Ongoing','Pause', 'On-Hold']
                },

                // Add toolbox
                toolbox: {
                    show: true,
                    orient: 'vertical',
                    x: 'right', 
                    y: 35,
                    feature: {
                        mark: {
                            show: false,
                            title: {
                                mark: 'Markline switch',
                                markUndo: 'Undo markline',
                                markClear: 'Clear markline'
                            }
                        },
                        magicType: {
                            show: false,
                            title: {
                                line: 'Switch to line chart',
                                bar: 'Switch to bar chart',
                                stack: 'Switch to stack',
                                tiled: 'Switch to tiled'
                            },
                            type: ['line', 'bar', 'stack', 'tiled']
                        },
                        restore: {
                            show: false,
                            title: 'Restore'
                        },
                        saveAsImage: {
                            show: false,
                            title: 'Same as image',
                            lang: ['Save']
                        }
                    }
                },

                // Enable drag recalculate
                calculable: true,

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: column_chart_data,
                    axisLabel:{interval:0, rotate:30}
                }],
                // Vertical axis
                yAxis: [{
                    type: 'value',
                    splitArea: {show: true}
                }],

                // Add series
                series: columnchart_array
            };

            // Connect charts
            connect_pie.connect(connect_column);
            connect_column.connect(connect_pie);

            connect_pie.setOption(connect_pie_options);
            connect_column.setOption(connect_column_options);



            // Resize charts
            // ------------------------------

            window.onresize = function () {
                setTimeout(function (){
                    connect_pie.resize();
                    connect_column.resize();
                }, 200);
            }
        }
    );
});
