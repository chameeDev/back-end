# back-end

This is a demo application and can use as a blueprint for creating a information management system.

## Getting started

1. Run composer install 

2. php artisan key:generate

3.Run php artisan migrate 

After you have completely configured the project you can log in to the system using below super admin credentials

**Email** : admin@admin.com
**Password** : admin@123

## Identified Bugs
1.User can delete his/her profile if the type is Admin and id is greater than one




