$(function(){
    
    window.setTimeout(function() {$(".alert-dismiss").slideUp(500, function(){ $(".alert-dismiss").alert('close'); });}, 3000);    
        
    $('.select2').select2({
        minimumResultsForSearch: 5,
        /*width: 'auto'*/
    });    
    
    $.ajaxSetup({
        cache: false,
        error: function(xhr, status, err) {
            if (xhr.status == 401){
                window.location.href = APP.siteUrl; 
            }
        }
    });

    $('.styled').uniform({ radioClass: 'choice' });

    $(window).on('scroll', function(){
        if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
            $('#return-to-top').fadeIn(200);    // Fade in the arrow
        } else {
            $('#return-to-top').fadeOut(200);   // Else fade out the arrow
        }
    });
    $('#return-to-top').click(function() {      // When arrow is clicked
        $('body,html').animate({
            scrollTop : 0                       // Scroll to top of body
        }, 500);
    });

    var switchery_w = Array.prototype.slice.call(document.querySelectorAll('.switchery-warning'));
    switchery_w.forEach(function(html) {
      var switchery = new Switchery(html, { color: '#FF7043' });
    });
    switchery_w = Array.prototype.slice.call(document.querySelectorAll('.switchery-success'));
    switchery_w.forEach(function(html) {
      var switchery = new Switchery(html);
    });
    switchery_w = Array.prototype.slice.call(document.querySelectorAll('.switchery-danger'));
    switchery_w.forEach(function(html) {
      var switchery = new Switchery(html, { color: '#EF5350' });
    });
    if($('.datepicker').length > 0){
        $('.datepicker').datepicker({format: "dd-M-yyyy",todayBtn: "linked",autoclose: true,todayHighlight: true});
    }

    getNotifications();

    setInterval(function(){ 
        getNotifications(); 
    }, 30000);


});

$(document).ajaxError(function (event, jqxhr, settings, thrownError) {
    if (jqxhr.status === 401) {
        console.log('Your session has expired. Please refresh the page.');
       // alert('Your session has expired. Please refresh the page.');
        //$.showSessionExpired();
        return false;
    }
});

$.fn.dataTable.ext.errMode = function (settings, techNote, message ) { 
    console.log(message);
};



getNotifications = function () {
    $.ajax({
        url: APP.siteUrl + 'notifications',
        type: "GET",
        dataType: 'json',
        success: function (data) {
            if(data.status){
                if(data.count > 0){
                    $('#li-notification-count').html(data.count);
                    $('#ul-notification-list').html('');
                    data.notifications.forEach(function(n){
                        var li = $('<li class="media">' + 
                                    '<div class="media-left">' +
                                        '<i class=" icon-quill2"></i>' +
                                    '</div>' +
                                    '<div class="media-body">' + 
                                    '<a href="' + (n.url ? n.url : '#') + '" class="media-heading">' + 
                                        '<span class="text-semibold">' + n.title + '</span>' + 
                                        '<span class="media-annotation pull-right">' + n.created_at + '</span>' +
                                    '</a>' + 
                                    '<span class="text-muted">' + n.message + '</span>' + 
                                '</div>' + 
                            '</li>');
                        $('#ul-notification-list').append(li);
                    });
                }else{
                    $('#li-notification-count').html('');
                    $('#ul-notification-list').html('');
                }
            }
        }
    });
}

markNotificationAsRead = function(type, id){
    let url = APP.siteUrl + 'notifications/' + type + '/' + id + '/read';
    $.ajax({
        type: 'POST',
        url: url,
        data: $('#form-notification-read').serialize(),
        dataType: "json",
        success: function(rtn)
        {
            if (rtn.status == true){
                getNotifications();
            }else{
                $.jGrowl_error('Failed', rtn.msg);
            }
        },
        error:function(jq, status, error){
            $.jGrowl_error('Failed','An error occured while updating.');
        }
    });
}

getFullDirList = function (dirs, initial_path){
    var list = [];
    dirs.forEach(function(d){
    	d.path = initial_path + d.name;
    	list.push(d);
    	if(d.sub_dirs && d.sub_dirs.length > 0){
            list = list.concat(getFullDirList(d.sub_dirs, d.path+'/'));
        }
    });
    return list;
}

escapeRegExp = function (string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

arrangeIntoTree = function (paths) {
    // Adapted from http://brandonclapp.com/arranging-an-array-of-flat-paths-into-a-json-tree-like-structure/
    var tree = [];

    for (var i = 0; i < paths.length; i++) {
        var path = paths[i];
        var currentLevel = tree;
        for (var j = 0; j < path.length; j++) {
            var part = path[j];
            var existingPath = findWhere(currentLevel, 'text', part);
            //var icon =  (j == path.length - 1 ? 'icon-file-text' : 'icon-folder-open');
            var icon = 'icon-file-text text-info';
            if (existingPath) {
                existingPath.icon = 'icon-folder-open3 text-warning-600';
                currentLevel = existingPath.children;
            } else {
                var newPart = {
                    text: part,
                    children: [],
                    icon: icon
                }

                currentLevel.push(newPart);
                currentLevel = newPart.children;
            }
        }
    }
    return tree;

    function findWhere(array, key, value) {
        // Adapted from https://stackoverflow.com/questions/32932994/findwhere-from-underscorejs-to-jquery
        t = 0; // t is used as a counter
        while (t < array.length && array[t][key] !== value) { t++; }; // find the index where the id is the as the aValue

        if (t < array.length) {
            return array[t]
        } else {
            return false;
        }
    }
}

convertDirStructureAsJsTree = function (structure){
    var rtn = [];
    structure.forEach(function(d){
        var nd = { text : d.name };
        if(d.sub_dirs){
            nd.children = convertDirStructureAsJsTree(d.sub_dirs);
            nd.state = {opened : true};
        }
        rtn.push(nd);
    });
    return rtn;
}

generateUUID = function () { // Public Domain/MIT
    var d = new Date().getTime();//Timestamp
    var d2 = (performance && performance.now && (performance.now()*1000)) || 0;//Time in microseconds since page-load or 0 if unsupported
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16;//random number between 0 and 16
        if(d > 0){//Use timestamp until depleted
            r = (d + r)%16 | 0;
            d = Math.floor(d/16);
        } else {//Use microseconds since page-load if supported
            r = (d2 + r)%16 | 0;
            d2 = Math.floor(d2/16);
        }
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return 'id-' + uuid;
}