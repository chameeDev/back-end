@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<div class="content">
		@include('common.errors')
        @include('common.success')
        <div class="row">
            <div class="col-md-12">
                @if (isset($employee))
                <form method="post" action="{{route('employees.update', $employee->id)}}" role='form' class="form-horizontal" id="form-entry" autocomplete="off">
                    <input type="hidden" name="_method" value="PATCH">
                @else
                <form method="post" action="{{route('employees.store')}}" role='form' class="form-horizontal" id="form-entry" autocomplete="off">
                @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="panel panel-white border-top-xlg border-top-info">
                    	<div class="panel-heading">
	            			@if (isset($employee))	
							<h5 class="panel-title">Edit Employee</h5>
							@else
							<h5 class="panel-title">Create Employee</h5>
							@endif
							<div class="heading-elements">
								<div class="heading-btn">
									<a href="{{ route('employees.index') }}" class="btn btn-xs btn-info btn-labeled"><b><i class="icon-backward"></i></b> Back to list</a>
								</div>
							</div>
						</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset class="content-group">
                                        <legend class="text-bold">Employee Detail</legend>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="user_type">Company</label>
                                            <div class="col-md-10">
                                                {{ Form::select('company_id', $companies, (isset($employee) ? $employee->company_id : ''), ['id' => 'company_id', 'class' => 'select2 full-width']) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="first_name">First Name</label>
                                            <div class="col-md-10">
                                                <input type="text" name="first_name" id="first_name" class="form-control" required maxlength="100" value="{{old('name', isset($employee)?$employee->first_name:NULL) }}" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="last_name">Last Name</label>
                                            <div class="col-md-10">
                                                <input type="text" name="last_name" id="last_name" class="form-control" required maxlength="100" value="{{old('name', isset($employee)?$employee->last_name:NULL) }}" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="email">Email</label>
                                            <div class="col-md-10">
                                                <input type="text" name="email" id="email" class="form-control" required maxlength="100" value="{{old('email', isset($employee)?$employee->email:NULL) }}" autocomplete="off"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="phone">Phone</label>
                                            <div class="col-md-10">
                                                <input type="text" name="phone" id="phone" class="form-control" maxlength="100" value="{{old('email', isset($employee)?$employee->phone:NULL) }}" autocomplete="off"/>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn bg-teal-400 btn-xs btn-labeled">
                                    <b><i class="icon-database-add"></i></b>
                                    Save Employee
                                </button>
                                @if(isset($employee))
                                @permission('delete.employee')
                                <button id="btn-delete" class="btn btn-xs btn-danger btn-labeled pull-right">
                                    <b><i class="icon-trash"></i></b>
                                    Delete Employee
                                </button>                            
                                @endpermission
                                @endif
                            </div>
                        </div>
                    </div>
                </form>                
            </div>
        </div>
	</div>
</div>
@if(isset($employee))
@permission('delete.employee')
<form method="POST" action="{{route('employees.destroy', $employee->id)}}" id="form-delete" class="hidden">
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>
@endpermission
@endif

@endsection

@section('content_script')
<style type="text/css">
  #map-canvas { height: 500px; margin-left: 15px; border: 1px solid #dddddd; border-radius: 4px;}
</style>
<script type="text/javascript">
    var url_employees_listing = '{{ route('employees.index') }}';

$(function(){
    $('input').on('keydown', function(event) {if (event.which == 13) {event.preventDefault();}});
    
    $('#form-entry').validate({
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        success: function(e) {
            e.closest('.form-group').removeClass('has-feedback');
            e.closest('label').remove();
        },
        errorPlacement: function(error, element) {
            if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {
            $.showWorking('Please wait...');
            $.ajax({
                type: 'POST',
                url: $(form).attr('action'),
                data: $(form).serialize(),
                dataType: "json",
                success: function(rtn)
                {
                    if (rtn.status == true){
                        window.location.href = url_employees_listing;
                    }   
                    else{
                        $.hideWorking();
                        alert(rtn.msg);
                    }
                },
                error:function(jq, status, error){
                    if(jq.status == 422){
                        $.hideWorking();
                        var errors = jq.responseJSON;
                        alert(errors);
                    }else{
                        $.hideWorking();
                        alert('An error occured while saving.');    
                    }
                }
            });
            return false;
        }
    });

    $('#btn-delete').confirmation({placement:'top', onConfirm:function(event, element){
        $.showWorking('Please wait...');
        $.ajax({
            type: 'POST',
            url: $('#form-delete').attr('action'),
            data: $('#form-delete').serialize(),
            dataType: "json",
            success: function(rtn)
            {
                if (rtn.status == true){
                    window.location.href = url_employees_listing;
                }else{
                    $.hideWorking();
                    alert(rtn.msg);
                }
            },
            error:function(jq, status, error){
                $.hideWorking();
                alert('An error occured while deleting.');
            }
        });
        return false;
    }});


});
</script>
@endsection