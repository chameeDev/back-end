@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold">Companies</span></h4>
			</div>
			@permission('create.company')
			<div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="{{ route('companies.create') }}" class="btn btn-xs btn-info btn-labeled" id="style-hover">
                        <b><i class="icon-add"></i></b>Create Company</a>
                </div>
            </div>
            @endpermission
		</div>
	</div>
	<!-- Content area -->
	<div class="content">
		@include('common.errors')
        @include('common.success')
		<div class="panel panel-flat">              
            <table class="table datatable-basic table-xxs table-striped" id="dataTables">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Website</th>
                        <th style="width: 140px;" class="text-center"><i class="fa fa-flash"></i></th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
            
        </div>
	</div>

</div>
@endsection

@section('content_script')

<script type="text/javascript">
    var url_companies_listing_data = '{{ route('companies.load_ajax') }}';

    $(document).ready(function() {
        
        $('#dataTables').DataTable({
            pageLength : 50,
            order: [[0, 'asc']],    
            processing: true,
            serverSide: true,
            searchDelay: 600,
            oLanguage: {
                    "sProcessing": '<span class="label label-flat label-rounded label-icon border-purple text-purple-600"><i class="icon-spinner9 spinner position-left"></i>  Loading...</span>',
                    sSearch: '<span>Filter : </span> _INPUT_',
                    sSearchPlaceholder: 'Type to filter...',
                    sLengthMenu: '<span>Show : </span> _MENU_',
                },
            ajax: url_companies_listing_data,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>r',
            language: {
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            columns: [
                {},
                {},
                {},
                { orderable: false, searchable: false}
            ]
        });
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });
    });
</script>
@endsection