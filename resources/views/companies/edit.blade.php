@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<div class="content">
		@include('common.errors')
        @include('common.success')
        <div class="row">
            <div class="col-md-12">
                @if (isset($company))
                <form method="post" action="{{route('companies.update', $company->id)}}" role='form' class="form-horizontal" id="form-entry" autocomplete="off">
                    <input type="hidden" name="_method" value="PATCH">
                @else
                <form method="post" action="{{route('companies.store')}}" role='form' class="form-horizontal" id="form-entry" autocomplete="off">
                @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="panel panel-white border-top-xlg border-top-info">
                    	<div class="panel-heading">
	            			@if (isset($company))	
							<h5 class="panel-title">Edit Company</h5>
							@else
							<h5 class="panel-title">Create Company</h5>
							@endif
							<div class="heading-elements">
								<div class="heading-btn">
									<a href="{{ route('companies.index') }}" class="btn btn-xs btn-info btn-labeled"><b><i class="icon-backward"></i></b> Back to list</a>
								</div>
							</div>
						</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset class="content-group">
                                        <legend class="text-bold">Company Detail</legend>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="name">Name</label>
                                            <div class="col-md-10">
                                                <input type="text" name="name" id="name" class="form-control" required maxlength="100" value="{{old('name', isset($company)?$company->name:NULL) }}" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="email">Email</label>
                                            <div class="col-md-10">
                                                <input type="text" name="email" id="email" class="form-control" required maxlength="100" value="{{old('email', isset($company)?$company->email:NULL) }}" autocomplete="off"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="website">Website</label>
                                            <div class="col-md-10">
                                                <input type="text" name="website" id="website" class="form-control" required maxlength="100" value="{{old('email', isset($company)?$company->website:NULL) }}" autocomplete="off"/>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn bg-teal-400 btn-xs btn-labeled">
                                    <b><i class="icon-database-add"></i></b>
                                    Save Company
                                </button>
                                @if(isset($company))
                                @permission('delete.company')
                                <button id="btn-delete" class="btn btn-xs btn-danger btn-labeled pull-right">
                                    <b><i class="icon-trash"></i></b>
                                    Delete Company
                                </button>                            
                                @endpermission
                                @endif
                            </div>
                        </div>
                    </div>
                </form>                
            </div>
        </div>
	</div>
</div>
@if(isset($company))
@permission('delete.company')
<form method="POST" action="{{route('companies.destroy', $company->id)}}" id="form-delete" class="hidden">
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>
@endpermission
@endif

@endsection

@section('content_script')
<style type="text/css">
  #map-canvas { height: 500px; margin-left: 15px; border: 1px solid #dddddd; border-radius: 4px;}
</style>
<script type="text/javascript">
    var url_companies_listing = '{{ route('companies.index') }}';

$(function(){
    $('input').on('keydown', function(event) {if (event.which == 13) {event.preventDefault();}});
    
    $('#form-entry').validate({
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        success: function(e) {
            e.closest('.form-group').removeClass('has-feedback');
            e.closest('label').remove();
        },
        errorPlacement: function(error, element) {
            if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {
            $.showWorking('Please wait...');
            $.ajax({
                type: 'POST',
                url: $(form).attr('action'),
                data: $(form).serialize(),
                dataType: "json",
                success: function(rtn)
                {
                    if (rtn.status == true){
                        window.location.href = url_companies_listing;
                    }   
                    else{
                        $.hideWorking();
                        alert(rtn.msg);
                    }
                },
                error:function(jq, status, error){
                    if(jq.status == 422){
                        $.hideWorking();
                        var errors = jq.responseJSON;
                        alert(errors);
                    }else{
                        $.hideWorking();
                        alert('An error occured while saving.');    
                    }
                }
            });
            return false;
        }
    });

    $('#btn-delete').confirmation({placement:'top', onConfirm:function(event, element){
        $.showWorking('Please wait...');
        $.ajax({
            type: 'POST',
            url: $('#form-delete').attr('action'),
            data: $('#form-delete').serialize(),
            dataType: "json",
            success: function(rtn)
            {
                if (rtn.status == true){
                    window.location.href = url_companies_listing;
                }else{
                    $.hideWorking();
                    alert(rtn.msg);
                }
            },
            error:function(jq, status, error){
                $.hideWorking();
                alert('An error occured while deleting.');
            }
        });
        return false;
    }});


});
</script>
@endsection