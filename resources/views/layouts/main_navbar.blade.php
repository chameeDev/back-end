<!-- Main navbar -->
    <div class="navbar navbar-default navbar-fixed-top header-highlight">
        <div class="navbar-header bg-white" style="height: 41px;">
            <a class="navbar-brand" href="{{ URL::to('/') }}" style="padding:6px 10px;" ><img src="{{ asset('assets/images/logo_light.png?ver=20190827') }}" alt="" style="height:30px;"></a>

            <ul class="nav navbar-nav visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
        </div>

        <div class="navbar-collapse collapse" id="navbar-mobile">
            <ul class="nav navbar-nav">
                <li><a class="sidebar-control sidebar-main-toggle hidden-xs" title="Show/hide panel"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                
                <?php
                    $role_name='';
                    $role = DB::table('roles')->where('id',Auth::user()->user_type)->first();
                    if(isset($role->name)){
                        $role_name=$role->name;
                    }
                    $timezone=Auth::user()->time_zone;
                ?>
                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown" title="Account : {{ Auth::user()->name }}">
                        <img src="{{ is_null(Auth::user()->avatar) ? asset('assets/images/avatar_male.png') : Storage::disk('public')->url(Auth::user()->avatar) }}" class="avatar">
                        <span>{{ Auth::user()->name }}</span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-key"></i> Change Password</a></li>
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-user"></i>&nbsp;&nbsp;&nbsp;&nbsp;{{$role_name}}</li>
                        @if(!is_null($timezone))
                        <li style="margin-top: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-watch2"></i>&nbsp;&nbsp;&nbsp;&nbsp;{{$timezone}}</li>
                        @endif 
                        <li class="divider"></li>
                        <li>
							<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="icon-switch2"></i> Logout</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
						</li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- /main navbar -->