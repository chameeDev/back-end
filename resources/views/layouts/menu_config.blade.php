<?php

    $primary_nav = [];

    $empty= array(
                'name' => '',
                'url' => '',
                'icon' => ''
            );

    $roles =array(
                'name' => 'Roles',
                'url' => route('roles.index'),
                'icon' => 'gi gi-tags'
            );
    $users= array(
                'name' => 'Users',
                'url' => route('users.index'),
                'icon' => 'gi gi-tags'
            );

    if(Auth::user()->hasPermission('view.company', true)){
         $primary_nav[] = [
            'name'  => 'Companies',
            'icon'  => 'icon-office',
            'url'   => route('companies.index')
        ];
    }
    if(Auth::user()->hasPermission('view.employee', true)){
         $primary_nav[] = [
            'name'  => 'Employees',
            'icon'  => 'icon-users4',
            'url'   => route('employees.index')
        ];
    }
    if( Auth::user()->hasPermission('view.role', true) && Auth::user()->hasPermission('view.user', true) ){
        $primary_nav[] = [
            'name' => 'Security',
            'icon' => 'icon-key',
            'right' => 1,
            'sub' => [
                (Auth::user()->hasPermission('view.role', true) ? $roles : $empty),
                (Auth::user()->hasPermission('view.user', true) ? $users : $empty)
            ]
        ];
    }

?>
@include('layouts.side_bar', ['primary_nav' => $primary_nav])