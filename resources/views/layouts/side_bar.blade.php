<!-- Main sidebar -->
<div class="sidebar sidebar-main sidebar-fixed">
	<div class="sidebar-content">

		<!-- Main navigation -->
		<div class="sidebar-category sidebar-category-visible">
			<div class="category-content no-padding">
				<?php 
					$mnu_rights = array();
					if(!isset($active_page)){
						$active_page = Request::url();
					}
				?>
				<ul class="navigation navigation-main navigation-accordion">
					<?php 
						$li_active = ($active_page == route('home')) ? ' class="active"' : '';
					 ?>
					<li<?php echo $li_active; ?>>
						<a href="{{ route('home') }}" >
							<i class="icon-home4"></i><span>Dashboard</span>
						</a>
					</li>
					<li class="navigation-divider"></li>
					
					<?php foreach( $primary_nav as $key => $link ) {
						$link_class = ''; $li_active  = ''; $menu_link  = '';

						// Get 1st level link's vital info
						$url        = (isset($link['url']) && $link['url']) ? $link['url'] : '#';
						$active     = (isset($link['url']) && ($active_page == $link['url'])) ? ' active' : '';
						$icon       = (isset($link['icon']) && $link['icon']) ? '<i class="' . $link['icon'] . '"></i>' : '';

						// Check if the link has a submenu
						if (isset($link['sub']) && $link['sub']) {
							// Since it has a submenu, we need to check if we have to add the class active
							// to its parent li element (only if a 2nd or 3rd level link is active)
							foreach ($link['sub'] as $sub_link) {
								if (in_array($active_page, $sub_link)) {
									$li_active = ' class="active"';
									break;
								}

								// 3rd level links
								if (isset($sub_link['sub']) && $sub_link['sub']) {
									foreach ($sub_link['sub'] as $sub2_link) {
										if (in_array($active_page, $sub2_link)) {
											$li_active = ' class="active"';
											break;
										}
									}
								}
							}

							$menu_link = 'has-ul';
						}

						// Create the class attribute for our link
						if ($active || $menu_link) {
							$link_class = ' class="'.$active .'"';
						}
						if ($active){
							$li_active = ' class="active"';
						}
						if(isset($link['right']) && ! isset($mnu_rights[$link['right']])){
							/*continue;*/
						}
					?>
					<?php if ($url == 'separator') { // if it is a separator and not a link ?>
					<li class="navigation-divider"></li>
					<?php } else if ($url == 'header'){ ?>
					<li class="navigation-header"><span><?php echo  $link['name']; ?></span> <i class="<?php echo  $link['icon']; ?>"></i></li>
					<?php } else { // If it is a link ?>
					<li<?php echo $li_active; ?><?php echo isset($link['id'])?' id="'.$link['id'].'"':'' ?>>
						<a href="<?php echo $url; ?>">
							<?php echo $icon; ?>
							<span>
								<?php echo  $link['name']; ?>
								<?php if(isset($link['notification'])){ ?>
								<span class="badge <?php echo $link['notification']['colour']; ?>" style="display: none;" id="<?php echo $link['notification']['id']; ?>">0</span>
								<?php } ?>
							</span>
						</a>

						<?php if (isset($link['sub']) && $link['sub']) { // if the link has a submenu ?>
						<ul>
							<?php foreach ($link['sub'] as $sub_link) {

								if($sub_link['name'] !=''){
								
								if(isset($sub_link['right']) && ! isset($mnu_rights[$sub_link['right']])){
									/*continue;*/
								}

								$link_class = '';
								$li_active = '';
								$submenu_link = '';

								// Get 2nd level link's vital info
								$url        = (isset($sub_link['url']) && $sub_link['url']) ? $sub_link['url'] : '#';
								$active     = (isset($sub_link['url']) && ($active_page == $sub_link['url'])) ? ' active' : '';

								// Check if the link has a submenu
								if (isset($sub_link['sub']) && $sub_link['sub']) {
									// Since it has a submenu, we need to check if we have to add the class active
									// to its parent li element (only if a 3rd level link is active)
									foreach ($sub_link['sub'] as $sub2_link) {
										if (in_array($active_page, $sub2_link)) {
											$li_active = ' class="active"';
											break;
										}
									}

									$submenu_link = 'has-ul';
								}

								if ($active || $submenu_link) {
									$link_class = ' class="'. $active .'"';
								}
							?>
							<li<?php echo $link_class; ?>>
								<a href="<?php echo $url; ?>">
									<?php echo isset($sub_link['icon']) ? '<i class="'.$sub_link['icon'].'"></i>' : '' ?>
									<?php echo  $sub_link['name']; ?>
								</a>
								<?php if (isset($sub_link['sub']) && $sub_link['sub']) { ?>
									<ul<?php echo ($link_class) ? '' : ' class="hidden-ul"' ?>>
										<?php foreach ($sub_link['sub'] as $sub2_link) {
											// Get 3rd level link's vital info
											$url    = (isset($sub2_link['url']) && $sub2_link['url']) ? $sub2_link['url'] : '#';
											$active = (isset($sub2_link['url']) && ($active_page == $sub2_link['url'])) ? ' class="active"' : '';
										?>
										<li<?php echo $li_active; ?>>
											<a href="<?php echo $url; ?>"><?php echo $sub2_link['name']; ?></a>
										</li>
										<?php } ?>
									</ul>
								<?php } ?>
							</li>
							<?php }} ?>
						</ul>
						<?php } ?>
					</li>
					<?php } ?>
					<?php } ?>

				</ul>
			</div>
		</div>
		<!-- /main navigation -->

	</div>
</div>
<!-- /main sidebar -->