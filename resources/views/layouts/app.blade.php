<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Backend">
    <meta name="author" content="Chameera">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Backend</title>
    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="{{ asset('1304-37.jpg') }}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/images/icons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/icons/favicon-16x16.png') }}">    
    
    <!-- END Icons -->
    <script type="text/javascript">
        var APP = (function (){
            var _baseUrl = "<?php echo asset(''); ?>";
            var _siteUrl = "<?php echo URL::to('/'); ?>/";
            var _userLogedIn = <?php echo Auth::check() ? 'true' : 'false';  ?>;
            return{
                "baseUrl" : _baseUrl,
                "siteUrl" : _siteUrl,
                "userLogedIn" : _userLogedIn,
                "googleApiKey" : ""
            }
        })();
    </script>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/core.min.css?ver=20180726') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/js/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/ajax-working.css') }}" rel="stylesheet" type="text/css">

    <style type="text/css">
    			.sidebar{
		background-color: #10416A !important;
		}

		.navigation>li.active>a, .navigation>li.active>a:focus, .navigation>li.active>a:hover{

		background-color: #C4D602 !important;
		}
    </style>
    
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{URL::asset('assets/js/plugins/pickers/datepicker.js')}}"></script>

		<script type="text/javascript" src="{{ asset('assets/js/plugins/visualization/c3/c3.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/plugins/visualization/d3/d3.min.js') }}"></script>

    @yield('content_css')
    
    <link href="{{ asset('assets/css/custom.css?ver=20180120') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
    
</head>
	<body class="navbar-top">
		@include('layouts.main_navbar')
		<!-- Page container -->
		<div class="page-container">
			<!-- Page content -->
			<div class="page-content">
				@include('layouts.menu_config')
		
				@yield('content')
			</div>
			<!-- /page content -->
		</div>

		<div id="modal-book-detail" class="modal fade" tabindex="-1"  data-backdrop="static">
		    <div class="modal-dialog modal-full">
		        <div class="modal-content" id="div-book-content">
		            
		        </div>
		    </div>
		</div>

		<!-- /page container -->
		<a href="javascript:" id="return-to-top"><i class="fa fa-chevron-up"></i></a>
		<!-- Scripts -->
		<!-- Core JS files -->
		<script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>

		<script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
		<!-- /core JS files -->

		<script type="text/javascript" src="{{ asset('assets/js/plugins/boostrap-multi-modal/bootstrap-modalmanager.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/plugins/boostrap-multi-modal/bootstrap-modal.js') }}"></script>

		<!-- Theme JS files -->
		<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
		
		<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/nicescroll.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/numeral.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/validation/validate.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jquery.bootstrap-growl.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/plugins/sweetalert/sweetalert.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/ajax-working.js?ver=20190227') }}"></script>

		<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/bootstrap-confirmation-2.4.0.js?v=20200421') }}"></script>

		<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
		<script src="{{URL::asset('assets/js/plugins/tables/datatables/extensions/fixed_columns.min.js')}}"></script>

		<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/knockout/knockout-3.2.0.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/knockout/knockout.mapping.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/plugins/visualization/echarts/echarts.js') }}"></script>
	
		<script type="text/javascript" src="{{ asset('assets/js/charts/echarts/echarts.js?ver=20190801') }}"></script>

		<script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/progressbar.min.js') }}"></script>

		<script type="text/javascript" src="{{ asset('assets/js/core/app.js?ver=20020325') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/core/layout_fixed_custom.js') }}"></script>
		<!-- /theme JS files -->
		
		@yield('content_script')
	</body>
</html>
