@extends('layouts.app')

@section('content')
	<!-- Content area -->
<div class="content-wrapper">	
	<div class="content">
		@include('common.errors')
        @include('common.success')

			<div class="row">
			<div class="col-md-6">
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h6 class="panel-title text-semibold">Graph1</h6>
						<div class="heading-elements">
							<ul class="icons-list">
								<li><a data-action="collapse"></a></li>
								<li><a data-action="reload"></a></li>
							</ul>
						</div>
					</div>

					<div class="panel-body">
						<div class="chart-container text-center">
							<div class="display-inline-block"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6" id="productivityUser">
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h6 class="panel-title text-semibold">Graph2</h6>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                	</ul>
	                	</div>
					</div>

					<div class="panel-body">
						
						<div class="chart-container">
							<div class="chart"></div>
						</div>
		
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
@endsection

@section('content_script')
<style>

</style>
<script type="text/javascript">

</script>
@endsection
