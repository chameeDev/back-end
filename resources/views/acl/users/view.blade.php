@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold">View User</span></h4>
			</div>
			<div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="{{ route('users.index') }}" class="btn btn-xs btn-info btn-labeled"><b><i class="icon-backward"></i></b> Back to list</a>
                </div>
            </div>
		</div>
	</div>

	<div class="content">
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-white border-top-xlg border-top-info">
					<div class="panel-heading">
						<h5 class="panel-title"><i class="icon-location4"></i> {{ $user->name }}</h5>
					</div>
					
					<div class="panel panel-body no-border no-margin-bottom">
						<div class="form-group">
							<label class="text-semibold">Email:</label>
							<span class="pull-right-sm">{{ $user->email }}</span>
						</div>
						<div class="form-group">
							<label class="text-semibold">Enabled:</label>
							<span class="pull-right-sm">{{ ($user->enabled)?'Yes':'No' }}</span>
						</div>
						<div class="form-group">
							<label class="text-semibold">Roles:</label>
							<span class="pull-right-sm">
								@if(!empty($user->roles))
			                    @foreach($user->roles as $v)
			                    <label class="label label-warning">{{ $v->name }}</label>
			                    @endforeach
			                    @endif
							</span>
						</div>
						<div class="form-group">
							<label class="text-semibold">Service Center:</label>
							<span class="pull-right-sm">{{ (isset($user->service_center->name))?$user->service_center->name:'All Service Centers' }}</span>
						</div>
						<div class="form-group">
							<label class="text-semibold">Last Login:</label>
							<span class="pull-right-sm">{{ $user->last_login_at }}</span>
						</div>
					</div>
					<hr class="no-margin">
					<div class="panel-body text-center alpha-grey">
						@permission('edit.user')
						<a href="{{route('users.edit', [$user->id])}}" class="btn btn-xs bg-green-400 btn-labeled">
							<b><i class="icon-pencil"></i></b>
							Edit User
						</a>
						@endpermission		
					</div>
				</div>
			</div>
			
		</div>
	</div>

</div>
@endsection

@section('content_script')

@endsection