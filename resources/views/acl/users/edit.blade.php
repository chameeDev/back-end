@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<div class="content">
		@include('common.errors')
        @include('common.success')
        <div class="row">
            <div class="col-md-12">
                @if (isset($user))
                <form method="post" action="{{route('users.update', $user->id)}}" role='form' class="form-horizontal" id="form-entry" autocomplete="off">
                    <input type="hidden" name="_method" value="PATCH">
                @else
                <form method="post" action="{{route('users.store')}}" role='form' class="form-horizontal" id="form-entry" autocomplete="off">
                @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="panel panel-white border-top-xlg border-top-info">
                    	<div class="panel-heading">
	            			@if (isset($user))	
							<h5 class="panel-title">Edit User</h5>
							@else
							<h5 class="panel-title">Create User</h5>
							@endif
							<div class="heading-elements">
								<div class="heading-btn">
									<a href="{{ route('users.index') }}" class="btn btn-xs btn-info btn-labeled"><b><i class="icon-backward"></i></b> Back to list</a>
								</div>
							</div>
						</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset class="content-group">
                                        <legend class="text-bold">User Detail</legend>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="name">Name</label>
                                            <div class="col-md-10">
                                                <input type="text" name="name" id="name" class="form-control" required maxlength="100" value="{{old('name', isset($user)?$user->name:NULL) }}" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="email">Email</label>
                                            <div class="col-md-10">
                                                <input type="text" name="email" id="email" class="form-control" required maxlength="100" value="{{old('email', isset($user)?$user->email:NULL) }}" autocomplete="off"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="user_type">User Role</label>
                                            <div class="col-md-10">
                                                {{ Form::select('user_role', $roles, (isset($user) ? $user->roles()->first()->id : ''), ['id' => 'user_role', 'class' => 'select2 full-width']) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="password">Password</label>
                                            <div class="col-md-10">
                                                <input type="password" name="password" id="password" class="form-control"  value="" autocomplete="new-password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="password_confirmation">Confirm Password</label>
                                            <div class="col-md-7">
                                                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" value="" autocomplete="new-password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="inactive">Inactive</label>
                                            <div class="col-md-4">
                                                <input type="checkbox" class="switchery-danger" name="inactive" id="inactive" <?php echo ( isset($user) && $user->inactive == 1 ? 'checked' : '') ?>>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn bg-teal-400 btn-xs btn-labeled">
                                    <b><i class="icon-database-add"></i></b>
                                    Save User
                                </button>
                                @if(isset($user))
                                @permission('delete.user')
                                <button id="btn-delete" class="btn btn-xs btn-danger btn-labeled pull-right">
                                    <b><i class="icon-trash"></i></b>
                                    Delete User
                                </button>                            
                                @endpermission
                                @endif
                            </div>
                        </div>
                    </div>
                </form>                
            </div>
        </div>
	</div>
</div>
@if(isset($user))
@permission('delete.user')
<form method="POST" action="{{route('users.destroy', $user->id)}}" id="form-delete" class="hidden">
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>
@endpermission
@endif

@endsection

@section('content_script')
<style type="text/css">
  #map-canvas { height: 500px; margin-left: 15px; border: 1px solid #dddddd; border-radius: 4px;}
</style>
<script type="text/javascript">
    var url_users_listing = '{{ route('users.index') }}';

$(function(){
    $('input').on('keydown', function(event) {if (event.which == 13) {event.preventDefault();}});
    
    $('#form-entry').validate({
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        success: function(e) {
            e.closest('.form-group').removeClass('has-feedback');
            e.closest('label').remove();
        },
        errorPlacement: function(error, element) {
            if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {
            $.showWorking('Please wait...');
            $.ajax({
                type: 'POST',
                url: $(form).attr('action'),
                data: $(form).serialize(),
                dataType: "json",
                success: function(rtn)
                {
                    if (rtn.status == true){
                        window.location.href = url_users_listing;
                    }   
                    else{
                        $.hideWorking();
                        alert(rtn.msg);
                    }
                },
                error:function(jq, status, error){
                    if(jq.status == 422){
                        $.hideWorking();
                        var errors = jq.responseJSON;
                        alert(errors);
                    }else{
                        $.hideWorking();
                        alert('An error occured while saving.');    
                    }
                }
            });
            return false;
        }
    });

    $('#btn-delete').confirmation({placement:'top', onConfirm:function(event, element){
        $.showWorking('Please wait...');
        $.ajax({
            type: 'POST',
            url: $('#form-delete').attr('action'),
            data: $('#form-delete').serialize(),
            dataType: "json",
            success: function(rtn)
            {
                if (rtn.status == true){
                    window.location.href = url_users_listing;
                }else{
                    $.hideWorking();
                    alert(rtn.msg);
                }
            },
            error:function(jq, status, error){
                $.hideWorking();
                alert('An error occured while deleting.');
            }
        });
        return false;
    }});


});
</script>
@endsection