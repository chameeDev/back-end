@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="text-semibold">Roles</span></h4>
            </div>
            @permission('create.role')
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="{{ route('roles.create') }}" class="btn btn-xs btn-info btn-labeled" id="style-hover">
                        <b><i class="icon-add"></i></b>Create Role</a>
                </div>
            </div>
            @endpermission
        </div>
    </div>
    <!-- Content area -->
    <div class="content">
        @include('common.errors')
        @include('common.success')
        <div class="panel panel-flat">              
            <table class="table datatable-basic table-xxs table-striped" id="dataTables">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th style="width: 140px;" class="text-center"><i class="fa fa-flash"></i></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($roles as $key => $role)
                    <tr>
                        <td>{{ $role->name }}</td>
                        <td>{{ $role->description }}</td>
                        <td>
                            <div class="btn-group">
                            <a class="btn border-info text-info-600 btn-flat btn-icon btn-xxs" href="{{ route('roles.view',$role->id) }}"><i class="icon-file-eye"></i></a>
                            @permission('edit.role')
                            <a class="btn border-warning text-warning-600 btn-flat btn-icon btn-xxs" href="{{ route('roles.edit',$role->id) }}"><i class="icon-pencil"></i></a>
                            @endpermission
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            
        </div>
    </div>

</div>
@endsection

@section('content_script')

@endsection