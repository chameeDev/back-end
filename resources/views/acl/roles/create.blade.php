@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold">
                    @if (isset($role))
                        Edit Role
                    @else
                        Create Role
                    @endif
                    </span>
                </h4>
			</div>
			<div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="{{ route('roles.index') }}" class="btn btn-xs btn-info btn-labeled"><b><i class="icon-backward"></i></b> Back to list</a>
                </div>
            </div>
		</div>
	</div>
	<!-- Content area -->
	<div class="content">
		@include('common.errors')
        @include('common.success')
		<div class="panel panel-flat">              
            <div class="row">
                <div class="col-md-12">
                    @if (isset($role))
                    <form method="post" action="{{route('roles.update', $role->id)}}" role='form' class="form-horizontal" id="form-entry">
                        <input type="hidden" name="_method" value="PATCH">
                    @else
                    <form method="post" action="{{route('roles.store')}}" role='form' class="form-horizontal" id="form-entry">
                    @endif
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="panel panel-flat">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="name">Name</label>
                                    <div class="col-md-7">
                                        <input type="text" name="name" id="name" class="form-control" required maxlength="100" value="{{old('name', isset($role)?$role->name:NULL) }}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="name">Slug</label>
                                    <div class="col-md-7">
                                        <input type="text" name="slug" id="slug" class="form-control" required maxlength="100" value="{{old('slug', isset($role)?$role->slug:NULL) }}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="description">Description</label>
                                    <div class="col-md-7">
                                        <input type="text" name="description" id="description" class="form-control" maxlength="100" value="{{old('description', isset($role)?$role->description:NULL)}}" >
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-md-2 control-label" >Permissions</label> 
                                    <div class="col-md-7">
                                        <ul id="tree_rights" class="easyui-tree1">
                                            <?php $name = ''; ?>
                                            @foreach($permission as $r)
                                            @if( $name != $r->name )
                                                @if( $name != '' )
                                                    </ul>
                                                    </li>
                                                @endif
                                                <li class="isFolder"> 
                                                    <span>{{ $r->description }}</span>
                                                    <ul>
                                                <?php $name = $r->name; ?>
                                            @endif
                                            <li id="{{$r->id}}" <?php echo isset($role) && in_array($r->id, $rolePermissions) ? 'checked' : ''; ?>>{{ $r->label }}</li>
                                            @endforeach
                                            </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-offset-2 col-md-3">
                                    <button type="submit" class="btn btn-success btn-xs">Save Group</button>
                                </div>
                                
                            </div>
                        </div>
                    </form>        
                </div>
            </div>            
        </div>
	</div>
</div>

@endsection

@section('content_script')

<script src="{{URL::asset('assets/js/plugins/ui/easytree/jquery.parser.js')}}"></script>
<script src="{{URL::asset('assets/js/plugins/ui/easytree/jquery.tree.js')}}"></script>
<link rel="stylesheet" href="{{URL::asset('assets/js/plugins/ui/easytree/tree.css')}}">
<script type="text/javascript">
    var url_roles_listing = '{{ route('roles.index') }}';

$(function(){
    $('input').on('keydown', function(event) {if (event.which == 13) {event.preventDefault();}});
    $('#tree_rights').tree({
        checkbox:true, 
        animate:true
    });

    $('#form-entry').validate({
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        success: function(e) {
            e.closest('.form-group').removeClass('has-feedback');
            e.closest('label').remove();
        },
        errorPlacement: function(error, element) {
            if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {
            var rights = $('#tree_rights').tree('getChecked');
            var data = $(form).serializeArray();
            $.each(rights , function(i, r){
                if(r.id){
                    data.push({name:'permission[]', value: r.id});
                }
            });
            $.showWorking('Please wait...');
            $.ajax({
                type: 'POST',
                url: $(form).attr('action'),
                data: data,
                dataType: "json",
                success: function(rtn)
                {
                    if (rtn.status == true){
                        window.location.href = url_roles_listing;
                    }else{
                        $.hideWorking();
                        alert(rtn.msg);
                    }
                },
                error:function(jq, status, error){
                    if(jq.status == 422){
                        $.hideWorking();
                        var errors = jq.responseJSON;
                        alert(errors);
                    }else{
                        $.hideWorking();
                        alert('An error occured while saving.');    
                    }
                }
            });
            return false;
        }
    });

    $('#btn-delete').confirmation({placement:'top', onConfirm:function(event, element){
        $.showWorking('Please wait...');
        $.ajax({
            type: 'POST',
            url: $('#form-delete').attr('action'),
            data: $('#form-delete').serialize(),
            dataType: "json",
            success: function(rtn)
            {
                if (rtn.status == true){
                    window.location.href = url_roles_listing;
                }else{
                    $.hideWorking();
                    alert(rtn.msg);
                }
            },
            error:function(jq, status, error){
                $.hideWorking();
                alert('An error occured while deleting.');
            }
        });
        return false;
    }});
});
</script>

@endsection