@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold">View User Group</span></h4>
			</div>
			<div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="{{ route('roles.index') }}" class="btn btn-xs btn-info btn-labeled"><b><i class="icon-backward"></i></b> Back to list</a>
                </div>
            </div>
		</div>
	</div>

	<div class="content">
		<div class="row">
			<div class="col-md-5">
				<div class="panel panel-white border-top-xlg border-top-info">
					<div class="panel-heading">
						<h5 class="panel-title"><i class="icon-key"></i> {{ $role->name }}</h5>
					</div>
					
					<div class="panel panel-body no-border no-margin-bottom">
						<div class="form-group">
							<label class="text-semibold">Description:</label>
							<span class="pull-right-sm">{{ $role->description }}</span>
						</div>
						
						<div class="form-group mt-5">
							<label class="text-semibold">Permissions:</label>
							<span class="pull-right-sm">
								@if(!empty($rolePermissions))
			                    <ul id="role_perms" class="list list-unstyled">
			                    <?php $name = ''; ?>
			                    @foreach($rolePermissions as $v)
			                    	@if( $name != $v->name )
                                        @if( $name != '' )
                                            </ul>
                                            </li>
                                        @endif
                                        <li> 
                                            <span>{{ $v->description }}</span>
                                            <ul>
                                        <?php $name = $v->name; ?>
                                    @endif
			                    <li>
			                    <span class="label border-left-success label-striped">{{ $v->label }}</span>
			                    </li>
			                    @endforeach
			                    		</ul>
                                    </li>
			                    </ul>
			                    @endif
							</span>
						</div>
					</div>
					<hr class="no-margin">
					<div class="panel-body text-center alpha-grey">
						@permission('edit.role')
						<a href="{{route('roles.edit', [$role->id])}}"  class="btn btn-xs bg-green-400 btn-labeled">
							<b><i class="icon-pencil"></i></b>
							Edit User Group
						</a>
						@endpermission		
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection