@if (count($errors) > 0)
<div class="alert alert-danger alert-styled-left alert-arrow-left" role="alert">
    <ul style="list-style-type: none;padding-left: 0px;">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif