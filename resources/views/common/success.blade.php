@if (session('success'))
<div class="alert alert-success alert-dismiss alert-styled-left alert-arrow-left alert-float" data-expires="5000">
	<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
    {{ session('success') }}
</div>
@endif