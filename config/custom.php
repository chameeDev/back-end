<?php

return [
    
    'app_url' => env('APP_URL', false),
    'app_secret' => env('API_SECRET', false)
];
