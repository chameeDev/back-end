<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function(){
    Route::post('event_message_p1', 'Api\EventController@testEventMessage');     
    Route::post('update-file-acceptance', 'Api\EventController@updateFileAcceptance');
     
    /*Event messages */  
    Route::post('notify-workflow-start', 'Api\EventController@notifyWorkflowStart');
    Route::post('notify-workflow-next', 'Api\EventController@notifyWorkflowNext'); 
    Route::post('notify-workflow-end', 'Api\EventController@notifyWorkflowEnd'); 
    Route::post('/notify-workflow-accept', 'Api\EventController@notifyWorkflowAccept');
    //Route::post('send-event-status', 'Api\EventController@sendEventMessage'); 
});

Route::prefix('v1')->group(function() {
    Route::post('/login', "Api\AuthController@login");
    Route::post('refresh_token', 'Api\AuthController@userRefreshToken');

    Route::post('/register', "Api\AuthController@register");
    //Route::get('testme', 'Api\AuthController@testAPIAuth'); 
    Route::middleware('auth:api')->get('testme', 'Api\AuthController@testAPIAuth'); 

    Route::middleware('auth:api')->post('file-submission', 'Api\EventController@file_submission');
    Route::middleware('auth:api')->post('update-file-status', 'Api\EventController@update_file_status');
    Route::middleware('auth:api')->post('file-acceptance', 'Api\EventController@file_acceptance');
     
     //----noneed---Route::middleware('auth:api')->get('/all', 'api\v1\user\UserController@index'); 
});

