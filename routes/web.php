<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\HomeController; 
use App\Http\Controllers\Web\RoleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* dashboard */



Route::middleware(['auth','acl'])->group(function () {

	/* Users */
	Route::get('/users', ['as'=> 'users.index','uses' => 'Web\UserController@index','can'=> 'view.user']);
	Route::get('/users/get-data', ['as'=> 'users.load_ajax','uses' => 'Web\UserController@get_list','can'=> 'view.user']);
	Route::get('/users/create', ['as'=> 'users.create','uses' => 'Web\UserController@create','can'=> 'create.user']);
	Route::post('/users/create', ['as'=> 'users.store','uses' => 'Web\UserController@store','can'=> 'create.user']);
	Route::get('/users/{id}/edit', ['as' => 'users.edit', 'uses' => 'Web\UserController@edit', 'can' => 'edit.user'])->where('id', '[0-9]+');
	Route::patch('/users/{id}/update', ['as' => 'users.update', 'uses' => 'Web\UserController@update', 'can' => 'edit.user'])->where('id', '[0-9]+');
	Route::delete('/users/{id}/delete', ['as' => 'users.destroy', 'uses' => 'Web\UserController@destroy', 'can' => 'delete.user'])->where('id', '[0-9]+');

	/* Roles */
	Route::get('/roles', ['as'=> 'roles.index','uses' => 'Web\RoleController@index','can'=> 'view.role']);
	Route::get('/roles/create', ['as'=> 'roles.create','uses' => 'Web\RoleController@create','can'=> 'create.role']);
	Route::post('/roles/create', ['as' => 'roles.store', 'uses' => 'Web\RoleController@store', 'can' => 'create.role']);
	Route::get('/roles/{id}', ['as' => 'roles.view', 'uses' => 'Web\RoleController@view', 'can' => 'view.role'])->where('id', '[0-9]+');
	Route::get('/roles/{id}/edit', ['as' => 'roles.edit', 'uses' => 'Web\RoleController@edit', 'can' => 'edit.role'])->where('id', '[0-9]+');
	Route::patch('/roles/{id}/update', ['as' => 'roles.update', 'uses' => 'Web\RoleController@update', 'can' => 'edit.role'])->where('id', '[0-9]+');

    /* Companies */
    Route::get('/companies', ['as'=> 'companies.index','uses' => 'Web\CompanyController@index','can'=> 'view.company']);
    Route::get('/companies/get-data', ['as'=> 'companies.load_ajax','uses' => 'Web\CompanyController@get_list','can'=> 'view.company']);
    Route::get('/companies/create', ['as'=> 'companies.create','uses' => 'Web\CompanyController@create','can'=> 'create.company']);
    Route::post('/companies/create', ['as'=> 'companies.store','uses' => 'Web\CompanyController@store','can'=> 'create.company']);
    Route::get('/companies/{id}/edit', ['as' => 'companies.edit', 'uses' => 'Web\CompanyController@edit', 'can' => 'edit.company'])->where('id', '[0-9]+');
    Route::patch('/companies/{id}/update', ['as' => 'companies.update', 'uses' => 'Web\CompanyController@update', 'can' => 'edit.company'])->where('id', '[0-9]+');
    Route::delete('/companies/{id}/delete', ['as' => 'companies.destroy', 'uses' => 'Web\CompanyController@destroy', 'can' => 'delete.company'])->where('id', '[0-9]+');

    /* Employees */
    Route::get('/employees', ['as'=> 'employees.index','uses' => 'Web\EmployeeController@index','can'=> 'view.employee']);
    Route::get('/employees/get-data', ['as'=> 'employees.load_ajax','uses' => 'Web\EmployeeController@get_list','can'=> 'view.employee']);
    Route::get('/employees/create', ['as'=> 'employees.create','uses' => 'Web\EmployeeController@create','can'=> 'create.employee']);
    Route::post('/employees/create', ['as'=> 'employees.store','uses' => 'Web\EmployeeController@store','can'=> 'create.employee']);
    Route::get('/employees/{id}/edit', ['as' => 'employees.edit', 'uses' => 'Web\EmployeeController@edit', 'can' => 'edit.employee'])->where('id', '[0-9]+');
    Route::patch('/employees/{id}/update', ['as' => 'employees.update', 'uses' => 'Web\EmployeeController@update', 'can' => 'edit.employee'])->where('id', '[0-9]+');
    Route::delete('/employees/{id}/delete', ['as' => 'employees.destroy', 'uses' => 'Web\EmployeeController@destroy', 'can' => 'delete.employee'])->where('id', '[0-9]+');
});

require __DIR__.'/auth.php';
