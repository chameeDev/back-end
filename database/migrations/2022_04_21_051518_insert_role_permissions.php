<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("INSERT INTO `permissions` (`id`, `inherit_id`, `name`, `slug`, `label`, `description`, `created_at`, `updated_at`) VALUES
        (1, NULL, 'role', '{\"view\":true}', 'View Roles', 'Role Administration', '2020-10-19 14:19:50', '2020-10-19 14:19:50'),
        (2, NULL, 'role', '{\"create\":true}', 'Create Roles', 'role', '2020-10-19 14:20:03', '2020-10-19 14:20:03'),
        (3, NULL, 'role', '{\"edit\":true}', 'Edit Roles', 'role', '2020-10-19 14:20:14', '2020-10-19 14:20:14'),
        (4, NULL, 'role', '{\"delete\":true}', 'Delete Roles', 'role', '2020-10-19 14:20:22', '2020-10-19 14:20:22'),
        (5, NULL, 'user', '{\"view\":true}', 'View Users', 'User Administration', '2020-10-19 14:45:48', '2020-10-19 14:45:48'),
        (6, NULL, 'user', '{\"create\":true}', 'Create Users', 'user', '2020-10-19 14:45:48', '2020-10-19 14:45:48'),
        (7, NULL, 'user', '{\"edit\":true}', 'Edit Users', 'user', '2020-10-19 17:01:57', '2020-10-19 17:01:57'),
        (8, NULL, 'user', '{\"delete\":true}', 'Delete Users', 'user', '2020-10-19 17:01:57', '2020-10-19 17:01:57')");

        DB::statement("INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `created_at`, `updated_at`) VALUES (1, 'Admin', 'admin', 'Manage administration privileges', NULL, '2020-09-23 02:01:46')");
        DB::statement("INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`) VALUES
            (null, 1, 1, '2019-11-14 22:54:12', '2019-11-14 22:54:12'),
            (null, 2, 1, '2019-11-14 22:54:12', '2019-11-14 22:54:12'),
            (null, 3, 1, '2019-11-14 22:54:12', '2019-11-14 22:54:12'),
            (null, 4, 1, '2019-11-14 22:54:12', '2019-11-14 22:54:12'),
            (null, 5, 1, '2019-11-14 22:54:12', '2019-11-14 22:54:12'),
            (null, 6, 1, '2019-11-14 22:54:12', '2019-11-14 22:54:12'),
            (null, 7, 1, '2019-11-14 22:54:12', '2019-11-14 22:54:12'),
            (null, 8, 1, '2019-11-14 22:54:12', '2019-11-14 22:54:12')");
        DB::statement("INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES(null, 1, 1, '2020-09-23 02:38:05', '2020-09-23 02:38:05')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
