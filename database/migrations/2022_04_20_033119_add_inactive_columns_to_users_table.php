<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('inactive')->default(false);
            $table->tinyInteger('user_type');
        });

        DB::statement("INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `inactive`, `created_at`, `updated_at`, `user_type`) VALUES (1, 'Admin', 'admin@admin.com', '', '', 0, NULL, '2019-06-27 20:49:31', 1)");
        
        /* default password is `password` */
        DB::statement('update users set password=\'$2y$10$4Pq3BVvyZ3FIJB/eGopE7.4ipcx4nISczzx5kE6RaD0s84aV91gK2\' where id=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
};
