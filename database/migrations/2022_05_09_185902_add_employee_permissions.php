<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("INSERT INTO `permissions` (`id`, `inherit_id`, `name`, `slug`, `label`, `description`, `created_at`, `updated_at`) VALUES
        (13, NULL, 'employee', '{\"view\":true}', 'View Employees', 'Employee Administration', '2020-10-19 14:19:50', '2020-10-19 14:19:50'),
        (14, NULL, 'employee', '{\"create\":true}', 'Create Employees', 'employee', '2020-10-19 14:20:03', '2020-10-19 14:20:03'),
        (15, NULL, 'employee', '{\"edit\":true}', 'Edit Employees', 'employee', '2020-10-19 14:20:14', '2020-10-19 14:20:14'),
        (16, NULL, 'employee', '{\"delete\":true}', 'Delete Employees', 'employee', '2020-10-19 14:20:22', '2020-10-19 14:20:22')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
