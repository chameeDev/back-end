<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         DB::statement("INSERT INTO `permissions` (`id`, `inherit_id`, `name`, `slug`, `label`, `description`, `created_at`, `updated_at`) VALUES
        (9, NULL, 'company', '{\"view\":true}', 'View Companies', 'Company Administration', '2020-10-19 14:19:50', '2020-10-19 14:19:50'),
        (10, NULL, 'company', '{\"create\":true}', 'Create Companies', 'company', '2020-10-19 14:20:03', '2020-10-19 14:20:03'),
        (11, NULL, 'company', '{\"edit\":true}', 'Edit Companies', 'company', '2020-10-19 14:20:14', '2020-10-19 14:20:14'),
        (12, NULL, 'company', '{\"delete\":true}', 'Delete Companies', 'company', '2020-10-19 14:20:22', '2020-10-19 14:20:22')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
