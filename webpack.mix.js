const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.js('resources/js/custom.js', 'public/assets/js/custom.js');

mix.js('resources/js/pages/users/index.js', 'public/assets/js/pages/users/index.js');
mix.js('resources/js/pages/users/edit.js', 'public/assets/js/pages/users/edit.js');

mix.js('resources/js/pages/roles/create.js', 'public/assets/js/pages/roles/create.js');

mix.js('resources/js/pages/projects-admin/projects_index.js', 'public/assets/js/pages/projects-admin/projects_index.js');
mix.js('resources/js/pages/projects-admin/projects_edit.js', 'public/assets/js/pages/projects-admin/projects_edit.js');
mix.js('resources/js/pages/projects-admin/product_edit.js', 'public/assets/js/pages/projects-admin/product_edit.js');

mix.js('resources/js/pages/files/index.js', 'public/assets/js/pages/files/index.js');
mix.js('resources/js/pages/files/view.js', 'public/assets/js/pages/files/view.js');

mix.js('resources/js/app.js', 'public/js').postCss('resources/css/app.css', 'public/css', [
    require('postcss-import'),
    require('tailwindcss'),
    require('autoprefixer'),
]);
