<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class UserType extends Enum
{
    const Admin = 1;
    const Manager = 2;
    const Employee = 3;
}
