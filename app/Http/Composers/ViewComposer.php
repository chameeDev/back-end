<?php

namespace App\Http\Composers;

use DB;
use Auth;
use Illuminate\View\View;
use App\Repositories\UserRepository;

class ViewComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $project_menus;
    protected $user_projects;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

    }
}