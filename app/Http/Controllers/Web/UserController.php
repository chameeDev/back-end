<?php

namespace App\Http\Controllers\Web;

use DB;
use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use App\Enums\UserType;

use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Datatables;

class UserController extends Controller
{
    /**
     * Manipulating the system's user module 
     *
     */

    //Display the users list
    public function index(Request $request) {
        return view('acl.users.index');
    }

    //AJAX function for getting users into a jquery datatable
    public function get_list(Request $request){
        $users = User::select(['users.name', 'users.email', 'users.inactive','users.user_type', 'users.id'])->where('id', '>', 1);
        
        return Datatables::of($users)
            ->filter(function ($query) use($request) {
                if (! empty($request->input('search.value'))){
                    $keyword = $request->input('search.value');
                    $user_types = collect(UserType::getKeys());
                    $user_types = $user_types->filter(function ($item) use ($keyword) {
                                        return false !== stristr($item, $keyword);
                                    });
                    $user_types_ids = [];
                    foreach ($user_types as $ut) {
                        $user_types_ids[] = UserType::getValue($ut);
                    }
                    $query->where(function($q) use($keyword, $user_types_ids) {
                        $q->Where('name', 'like', "%{$keyword}%")
                            ->orWhere('email', 'like', "%{$keyword}%")
                            ->orWhereIn('user_type', $user_types_ids);
                    });
                }
            })
            ->removeColumn('id')
            ->editColumn('inactive', function($v) {
                return ($v->inactive == 1) ? '<label class="label label-danger">Yes</label>' : 'No';
            })
            ->editColumn('user_type', function($r) {
                $role = DB::table('roles')->where('id', $r->user_type)->first();
                return $role->name;
            })
            ->addColumn('action', function($r) {
                return '<div class="btn-group col-md-push-3">
                        <a href="'.route('users.edit', $r->id).'" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Edit</a>
                        </div>';
            })
            ->rawColumns([2, 3, 4])
            ->make(false);
    }

    //Display the page to create a new user 
    public function create() {
        $active_page = route('users.index');
        $user_types = UserType::toSelectArray();
        
        $roles = Role::pluck('name', 'id')->toArray();
        return view('acl.users.edit', compact('active_page', 'user_types', 'roles'));
    }

    //Add a new user into the database
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'name'       => 'required',
            'email'      => 'nullable|email',
            'password'   => ['required', 'min:8', 'confirmed', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/'],
            'status'     => '',
            'user_role'  => 'required|integer'
        ]);
        
        if ($validator->fails()) {
            return response()->json(['status' => FALSE, 'msg' => implode('<br>', $validator->errors()->all())]);
        }

        $input = $request->all();
        $input['password'] = \Hash::make($input['password']);

        $role = Role::find($input['user_role']);
        if (!$role) {
            return response()->json(['status' => FALSE, 'msg' => 'Invalid user Role']);
        }

        $user = new User();
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->password = $input['password'];
        $user->inactive = (isset($input['inactive']) ? 1 : 0);
        $user->user_type = $role->id;

        try {

            $user->save();

            $user->assignRole($role->id);
            
            \Session::flash('success', 'User created successfully.'); 
            return response()->json(['status' => TRUE, 'msg' => 'User created successfully.']);    
        } catch (Exception $e) {
            return response()->json(['status' => FALSE, 'msg' => 'Error occured while saving...']);
        }
    }

    //Display a particular user
    public function view($id) {
        $user = User::findOrFail($id);
        $active_page = route('users.index');
        return view('acl.users.view', compact('user', 'active_page'));
    }

    //Display a particular user for editing 
    public function edit($id) {
        $user = User::findOrFail($id);
        
        $roles = Role::pluck('name', 'id')->toArray();
        $user_types = UserType::toSelectArray();
        
        $active_page = route('users.index');
        return view('acl.users.edit', compact('user', 'roles', 'active_page', 'user_types'));
    }

    //Update certain attributes of a particular user 
    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'nullable|email',
            'password' => 'nullable|min:4|confirmed',
            'password_confirmation' => 'nullable',
            'user_role'  => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => FALSE, 'msg' => implode('<br>', $validator->errors()->all())]);
        }
        
        $input = $request->all();
        
        
        $role = Role::find($input['user_role']);
        if (!$role) {
            return response()->json(['status' => FALSE, 'msg' => 'Invalid user Role']);
        }
        
        $user = User::findOrFail($id);

        $user->name = $input['name'];
        $user->email = $input['email'];
        if (!empty($input['password'])) {
            $user->password = \Hash::make($input['password']);
        }
        $user->inactive =  (isset($input['inactive']) ? 1 : 0);
        $user->user_type = $role->id;

        try {

            $user->save();
            
            $user->revokeAllRoles();
            $user->assignRole($role->id);

            \Session::flash('success', 'User updated successfully.'); 
            return response()->json(['status' => TRUE, 'msg' => 'User updated successfully.']);    
        } catch (Exception $e) {
            return response()->json(['status' => FALSE, 'msg' => 'Error occured while deleting...']);
        }
    }

    //Deleting a particular user
    public function destroy($id) {
        $user = User::findOrFail($id);
        if($user->id == 1){
            return response()->json(['status' => False, 'msg' => 'Super admin can not be deleted.']);
        }
        DB::table('permission_user')->where('user_id', $user->id)->delete();

        $user->delete();
        \Session::flash('success', 'User deleted successfully.'); 
        return response()->json(['status' => TRUE, 'msg' => 'User deleted successfully.']);
    }
}
