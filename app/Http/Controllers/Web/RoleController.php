<?php

namespace App\Http\Controllers\Web;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kodeine\Acl\Models\Eloquent\Role;
use Kodeine\Acl\Models\Eloquent\Permission;
use DB;

class RoleController extends Controller
{
    /**
     * Define the methods for manipulating user roles.
     *
     */

    //Get the list of roles and display them
    public function index(Request $request) {
        $roles = Role::orderBy('id', 'ASC')->get();
        return view('acl.roles.index', compact('roles'));
    }

    //Display the page to create a new role
    public function create() {
        $permission = Permission::orderBy('name')->orderBy('id')->get();
        $active_page = route('roles.index');
        return view('acl.roles.create', compact('permission', 'active_page'));
    }

    //Add a new role to the database
    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:roles,name',
            'description' => '',
            'permission' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => FALSE, 'msg' => implode('<br>', $validator->errors()->all())]);
        }

        $role = new Role();
        $role->name = $request->input('name');
        $role->slug = $request->input('slug');
        $role->description = $request->input('description');
        $role->save();

        foreach ($request->input('permission') as $key => $value) {
            $role->assignPermission($value);
        }
        
        \Session::flash('success', 'Role created successfully.'); 
        return response()->json(['status' => TRUE, 'msg' => 'Role created successfully.']);
    }

    //Display a particular role 
    public function view($id) {
        $role = Role::findOrFail($id);
        $rolePermissions = Permission::join("permission_role", "permission_role.permission_id", "=", "permissions.id")
                ->where("permission_role.role_id", $id)
                ->orderBy('permissions.name')->orderBy('permissions.id')
                ->get();
        $active_page = route('roles.index');
        return view('acl.roles.view', compact('role', 'rolePermissions', 'active_page'));
    }

    //Display a particular role for editing purpose 
    public function edit($id) {
        $role = Role::findOrFail($id);
        $permission = Permission::orderBy('name')->orderBy('id')->get();
        $rolePermissions = DB::table("permission_role")->where("permission_role.role_id", $id)
                ->pluck('permission_role.permission_id', 'permission_role.permission_id')->toArray();

        $active_page = route('roles.index');
        return view('acl.roles.create', compact('role', 'permission', 'rolePermissions', 'active_page'));
    }

    //Update attributes of a particular role
    public function update(Request $request, $id) {
        $this->validate($request, [
           'name' => 'required|unique:roles,name,'.$id,
            'description' => '',
            'permission' => 'required',
        ]);

        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->slug = $request->input('slug');
        $role->description = $request->input('description');
        $role->save();

        $role->revokeAllPermissions();
        foreach ($request->input('permission') as $key => $value) {
            $role->assignPermission($value);
        }

        \Session::flash('success', 'Role updated successfully.'); 
        return response()->json(['status' => TRUE, 'msg' => 'Role updated successfully.']);
    }

    //Deleting a particular role
    public function destroy($id) {
        $role = Role::findOrFail($id);
        if($role->is_default){
            return response()->json(['status' => False, 'msg' => 'Default roles can not be deleted.']);
        }
        if(DB::table("role_user")->where('role_id', $id)->count() > 0){
            return response()->json(['status' => False, 'msg' => 'Roles is assigned to users. Can not be deleted.']);
        }
        
        DB::table("roles")->where('id', $id)->delete();
        \Session::flash('success', 'Role deleted successfully.'); 
        return response()->json(['status' => true, 'msg' => 'Role deleted successfully.']);
    }
}
