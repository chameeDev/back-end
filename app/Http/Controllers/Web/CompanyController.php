<?php

namespace App\Http\Controllers\Web;

use DB;
use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Enums\UserType;

use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Datatables;

class CompanyController extends Controller
{
    /**
     * Manipulating the system's company module 
     *
     */

    //Display the companies list
    public function index(Request $request) {
        return view('companies.index');
    }

    //AJAX function for getting companies into a jquery datatable
    public function get_list(Request $request){
        $companies = Company::select(['companies.name', 'companies.email', 'companies.website', 'companies.id']);
        
        return Datatables::of($companies)
            ->filter(function ($query) use($request) {
                if (! empty($request->input('search.value'))){
                    $keyword = $request->input('search.value');
                    
                    $query->where(function($q) use($keyword) {
                        $q->Where('name', 'like', "%{$keyword}%")
                            ->orWhere('email', 'like', "%{$keyword}%");
                    });
                }
            })
            ->removeColumn('id')
            ->addColumn('action', function($r) {
                return '<div class="btn-group col-md-push-3">
                        <a href="'.route('companies.edit', $r->id).'" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Edit</a>
                        </div>';
            })
            ->rawColumns([3])
            ->make(false);
    }

    //Display the page to create a new company 
    public function create() {
        $active_page = route('companies.index');
        return view('companies.edit', compact('active_page'));
    }

    //Add a new company into the database
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'name'       => 'required',
            'email'      => 'nullable|email'
        ]);
        
        if ($validator->fails()) {
            return response()->json(['status' => FALSE, 'msg' => implode('<br>', $validator->errors()->all())]);
        }

        $input = $request->all();

        $company = new Company();
        $company->name = $input['name'];
        $company->email = $input['email'];
        $company->website = $input['website'];

        DB::beginTransaction();
        try {

            $company->save();

            DB::commit();

            \Session::flash('success', 'Company created successfully.'); 
            return response()->json(['status' => TRUE, 'msg' => 'Company created successfully.']);    
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['status' => FALSE, 'msg' => 'Error occured while saving...']);
        }
    }

    //Display a particular company
    public function view($id) {
        $company = Company::findOrFail($id);
        $active_page = route('users.index');
        return view('companies.view', compact('company', 'active_page'));
    }

    //Display a particular company for editing 
    public function edit($id) {
        $company = Company::findOrFail($id);
        
        $active_page = route('companies.index');
        return view('companies.edit', compact('company','active_page'));
    }

    //Update certain attributes of a particular company 
    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'nullable|email'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => FALSE, 'msg' => implode('<br>', $validator->errors()->all())]);
        }
        
        $input = $request->all();
        
        $company = Company::findOrFail($id);

        $company->name = $input['name'];
        $company->email = $input['email'];
        $company->website = $input['website'];

        try {

            $company->save();
            

            \Session::flash('success', 'Company updated successfully.'); 
            return response()->json(['status' => TRUE, 'msg' => 'Company updated successfully.']);    
        } catch (Exception $e) {
            return response()->json(['status' => FALSE, 'msg' => 'Error occured while saving...']);
        }
    }

    //Deleting a particular company
    public function destroy($id) {
        $company = Company::findOrFail($id);

        $company->delete();
        \Session::flash('success', 'Company deleted successfully.'); 
        return response()->json(['status' => TRUE, 'msg' => 'Company deleted successfully.']);
    }
}
