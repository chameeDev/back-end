<?php

namespace App\Http\Controllers\Web;

use DB;
use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\Company;
use App\Enums\UserType;

use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Datatables;

class EmployeeController extends Controller
{
    /**
     * Manipulating the system's employee module 
     *
     */

    //Display the employees list
    public function index(Request $request) {
        return view('employees.index');
    }

    //AJAX function for getting employees into a jquery datatable
    public function get_list(Request $request){
        $employees = Employee::select(['companies.name', 'employees.first_name', 'employees.last_name', 'employees.email', 'employees.phone','employees.id'])->join('companies', 'companies.id', '=', 'employees.company_id');
        
        return Datatables::of($employees)
            ->filter(function ($query) use($request) {
                if (! empty($request->input('search.value'))){
                    $keyword = $request->input('search.value');
                    
                    $query->where(function($q) use($keyword) {
                        $q->Where('name', 'like', "%{$keyword}%")
                            ->orWhere('email', 'like', "%{$keyword}%");
                    });
                }
            })
            ->removeColumn('id')
            ->addColumn('action', function($r) {
                return '<div class="btn-group col-md-push-3">
                        <a href="'.route('employees.edit', $r->id).'" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Edit</a>
                        </div>';
            })
            ->rawColumns([5])
            ->make(false);
    }

    //Display the page to create a new employee 
    public function create() {
        $active_page = route('employees.index');
        $companies = Company::pluck('name', 'id')->toArray();
        return view('employees.edit', compact('active_page','companies'));
    }

    //Add a new employee into the database
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'company_id'       => 'required',
            'first_name'       => 'required',
            'last_name'       => 'required',
            'email'      => 'nullable|email'
        ]);
        
        if ($validator->fails()) {
            return response()->json(['status' => FALSE, 'msg' => implode('<br>', $validator->errors()->all())]);
        }

        $input = $request->all();

        $employee = new Employee();
        $employee->company_id  = $input['company_id'];
        $employee->first_name = $input['first_name'];
        $employee->last_name = $input['last_name'];
        $employee->phone = $input['phone'];
        $employee->email = $input['email'];

        try {

            $employee->save();

            \Session::flash('success', 'Employee created successfully.'); 
            return response()->json(['status' => TRUE, 'msg' => 'Employee created successfully.']);    
        } catch (Exception $e) {
            return response()->json(['status' => FALSE, 'msg' => 'Error occured while saving...']);
        }
    }

    //Display a particular employee
    public function view($id) {
        $employee = Employee::findOrFail($id);
        $active_page = route('employees.index');
        return view('employees.view', compact('employee', 'active_page'));
    }

    //Display a particular employee for editing 
    public function edit($id) {
        $employee = Employee::findOrFail($id);
        $companies = Company::pluck('name', 'id')->toArray();
        $active_page = route('employees.index');
        return view('employees.edit', compact('employee','active_page','companies'));
    }

    //Update certain attributes of a particular employee 
    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'company_id'       => 'required',
            'first_name'       => 'required',
            'last_name'       => 'required',
            'email'      => 'nullable|email'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => FALSE, 'msg' => implode('<br>', $validator->errors()->all())]);
        }
        
        $input = $request->all();
        
        $employee = Employee::findOrFail($id);

        $employee->company_id  = $input['company_id'];
        $employee->first_name = $input['first_name'];
        $employee->last_name = $input['last_name'];
        $employee->phone = $input['phone'];
        $employee->email = $input['email'];

        try {

            $employee->save();

            \Session::flash('success', 'Employee updated successfully.'); 
            return response()->json(['status' => TRUE, 'msg' => 'Employee updated successfully.']);    
        } catch (Exception $e) {
            return response()->json(['status' => FALSE, 'msg' => 'Error occured while saving...']);
        }
    }

    //Deleting a particular employee
    public function destroy($id) {
        $employee = Employee::findOrFail($id);

        $employee->delete();
        \Session::flash('success', 'Employee deleted successfully.'); 
        return response()->json(['status' => TRUE, 'msg' => 'Employee deleted successfully.']);
    }
}
